import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../guards/auth.guard';
import {SenderResolveService} from '../resolvers/sender-resolver.service';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthPageModule'
    },
    {
        path: 'account',
        canActivate: [AuthGuard],
        loadChildren: './account/account.module#AccountPageModule',
        resolve: {
            sender: SenderResolveService
        },
    },
    {
        path: '**',
        redirectTo: '/'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
