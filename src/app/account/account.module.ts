import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {AccountPage} from './account.page';
import {SharedModule} from '../../shared/shared.module';
import {ChatComponent} from './chat/chat.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from '../../guards/auth.guard';
import {RecipientResolveService} from '../../resolvers/recipient-resolver.service';

const routes: Routes = [
    {
        path: '',
        component: AccountPage,
        children: [
            {
                path: '',
                component: DashboardComponent
            },
            {
                path: 'chat/:id',
                component: ChatComponent,
                resolve: {
                    recipient: RecipientResolveService
                },
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [AccountPage, ChatComponent, DashboardComponent]
})
export class AccountPageModule {
}
