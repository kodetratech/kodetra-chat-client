import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ActivationStart, Router, RouterOutlet} from '@angular/router';
import {StorageService} from '../../services/storage.service';
import {UserService} from '../../services/user.service';
import {AuthenticationService} from '../../services/authentication.service';
import {SpinnerLoaderService} from '../../services/spinner-loader.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
    @ViewChild(RouterOutlet, {static: true}) outlet: RouterOutlet;
    public appPages = [
        // {
        //     title: 'Home',
        //     url: '/account/chat',
        //     icon: 'home'
        // },
    ];


    constructor(private router: Router,
                private route: ActivatedRoute,
                private userService: UserService,
                private spinnerLoaderService: SpinnerLoaderService,
                private authenticationService: AuthenticationService,
                private storageService: StorageService) {
        this.loadFriendList();
        this.route.data
            .subscribe((data: { sender: any }) => {
                console.log('view sender data', data);
                this.userService.setUserProfile(data.sender.data);
            });

    }


    loadFriendList() {
        this.spinnerLoaderService.startSpinner('Loading profile...Please wait').then(() => {
            this.storageService.getData('kodetra_chat_user').then((user) => {
                console.log('user', user);
                this.userService.setUser(user);
                this.authenticationService.getUsersList(this.userService.getUserId()).subscribe((response) => {
                    console.log('user list', response.data);
                    this.spinnerLoaderService.stopSpinner();
                    if (response.data) {
                        this.userService.setConnections(response.data);
                        response.data.forEach((user) => {
                            this.appPages.push({
                                title: user.full_name,
                                url: user.user._id,
                                icon: user.user.profilePicLink
                            });
                        });
                        this.router.navigateByUrl('/account');
                    }
                }, (error) => {
                    console.log('error user list', error);
                    this.spinnerLoaderService.stopSpinner();
                });
            });
        }).catch(() => {
            this.spinnerLoaderService.stopSpinner();
        });

    }

    public onRouterOutletActivate(event: any) {
        console.log(event);
    }


    openChat(userId) {
        this.userService.setChatRecipient(this.userService.getConnection(userId));
        this.router.navigateByUrl('/account/chat/' + userId);
    }

    ngOnInit() {
        // this.router.events.subscribe(e => {
        //     console.log('event e', e);
        //     if (e instanceof ActivationStart && e.snapshot.outlet === 'administration') {
        //         this.outlet.deactivate();
        //     }
        // });
    }

    logout() {
        // this.menuCtrl.enable(false);
        localStorage.clear();
        this.router.navigateByUrl('auth/login');
        this.storageService.clearStore();
    }
}
