import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {UserService} from '../../../services/user.service';
import {SocketService} from '../../../services/socket.service';
import {ChatService} from '../../../services/chat.service';
import {AuthenticationService} from '../../../services/authentication.service';


@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {
    // https://github.com/HsuanXyz/ionic3-chat
    // https://github.com/smukov/AvI/blob/master/Ionic2/src/components/chatBubble/chatBubble.ts
    private chatRoom: any;
    private currentGroupId: any;
    public senderUser: any;
    public receiverUser: any;
    public roomName: any;
    public userInput: any = '';
    public messages: Array<any> = [];
    public gridImages: Array<any> = [];
    public images: Array<any> = [];
    public conversationId: any = null;
    public recipientId: any;

    constructor(private userService: UserService,
                private route: ActivatedRoute,
                private router: Router,
                private authenticationService: AuthenticationService,
                private chatService: ChatService,
                public modalController: ModalController,
                private actionSheetCtrl: ActionSheetController,
                private socketService: SocketService) {
        console.log('chat constructor');
        this.recipientId = this.route.snapshot.paramMap.get('id');
        this.route.data
            .subscribe((data: { recipient: any }) => {
                console.log('view recipient data', data);
                this.receiverUser = data.recipient.data;
            });
        this.senderUser = this.userService.getUser();
        this.receiverUser = this.userService.getChatRecipient();

        // this.receiverUser = {
        //     user: {
        //         _id: this.senderUser._id,
        //     },
        //     full_name: 'Yogesh',
        //     name: 'Yogesh'
        // };
        console.log('receiverUser' + this.receiverUser);
    }

    loadPreviousChat(sender, recipient) {
        const request = {
            sender,
            recipient
        };
        this.messages = [];
        this.chatService.getPreviousChat(request).subscribe((response) => {
            console.log('Previous chat response', response);
            console.log('Previous chat this.senderUser', this.senderUser);
            response.data.forEach((message) => {
                let position;
                const profilePic = message.sender[0].profilePicLink;
                console.log('Previous chat this.senderUser == message sender', this.senderUser._id === message.sender[0]._id);
                if (this.senderUser._id === message.sender[0]._id) {
                    position = 'right';
                } else {
                    position = 'left';
                }
                const msg = {
                    img: profilePic,
                    content: message.messageText,
                    position,
                    time: message.created_at,
                };
                console.log('Previous chat msg', msg);
                this.messages.push(msg);
            });
        }, (error) => {
            console.log('Previous chat response', this.receiverUser);
        });
    }

    async loadConnectionProfile() {
        // const modal = await this.modalController.create({
        //     component: ProfileDetailsComponent,
        //     componentProps: {profile: this.receiverUser, type: 2}
        // });
        // modal.onDidDismiss().then((response: any) => {
        //     if (response.data !== null) {
        //         console.log('The result:', response);
        //     }
        // });
        // return await modal.present();
    }


    // loadConnectionProfile() {
    //     this.userService.setConnectionProfile(this.receiverUser);
    //     this.router.navigateByUrl('/account/connections/connection-profile');
    // }

    ionViewDidEnter() {
        console.log('chat ionViewDidEnter');
        this.socketService.initPrivateChatSocket();
        console.log('Recipient User', this.receiverUser);
        console.log('Private chat sender User', JSON.stringify(this.senderUser));
        console.log('Private chat receiver User', JSON.stringify(this.receiverUser));
        this.roomName = this.receiverUser.full_name;
        this.loadPreviousChat(this.senderUser._id, this.receiverUser.user._id);
        this.socketService.connection().subscribe((response) => {
            // console.log('Private chat connection successfull' + JSON.stringify(response));
            this.joinPrivateChatGroup(this.senderUser, this.receiverUser);
        }, (error) => {
            console.log('Private chat connection unsuccessfull', error);
        });

        this.socketService.privateGroupJoined().subscribe((response) => {
            // console.log('Private chat Group joined' + JSON.stringify(response));
            // this.currentGroupId = response[0].groupId;
            this.chatRoom = response;
            console.log('Chat Room Information : ' + JSON.stringify(this.chatRoom));
            // this.messageSqliteProvider.loadPrivateMessages(0, this.senderUser.userObjectId, this.receiverUser.userId, 0)
            //   .then((resultSet) => {
            //     console.log('load private message from the sqlite database', JSON.stringify(resultSet));
            //     if (resultSet.rows.length > 0) {
            //       console.log('if statement true');
            //       for (let i = 0; i < resultSet.rows.length; i++) {
            //         let position = 'left';
            //         let sender = JSON.parse(resultSet.rows.item(i).sender);
            //         console.log('Sender information' + JSON.stringify(sender));
            //         let profilePic = sender.profilePicLink;
            //         if (this.senderUser.userObjectId === resultSet.rows.item(i).senderId) {
            //           position = 'right';
            //         }
            //         let msg = {
            //           img: profilePic,
            //           content: resultSet.rows.item(i).messageText,
            //           position: position,
            //           time: resultSet.rows.item(i).created_at,
            //         };
            //         this.messages.push(msg);
            //         console.log('Message List' + JSON.stringify(this.messages));
            //       }
            //     } else {
            //       console.log('No Message for Found!!');
            //       this.messages = [];
            //     }
            //   })
            //   .catch((error) => {
            //     console.log('Error while load private message from sqlite database', error);
            //     this.messages = [];
            //   });
            // this.groupSqliteProvider.addNewGroup(response)
        }, (error) => {
            console.log('Private chat Group join  error', error);
        });

        this.socketService.receivePrivateChatMessage().subscribe((response) => {
            console.log('Receive Private Chat Message successfull', JSON.stringify(response));
            this.conversationId = response.conversationId;
            let position = 'left';
            let profilePic = response.senderProfilePic;
            if (this.senderUser._id === response.senderId) {
                position = 'right';
                profilePic = this.senderUser.profilePicLink;
            } else {
                profilePic = response.recipient.profilePicLink;
            }
            this.messages.push({
                img: profilePic,
                content: response.messageText,
                position,
                time: response.created_at,
            });
            // this.messageSqliteProvider.addNewMessage(response);
        }, (error) => {
            console.log('Error Receive Private Chat Message', error);
        });
    }


    joinPrivateChatGroup(sender, receiver) {
        const roomInfo = {
            room1: sender._id + '_' + receiver.user._id,
            room2: receiver.user._id + '_' + sender._id,
            group: {
                groupName: sender.phone + '_' + receiver.phone,
                groupDescription: '',
                groupMembers: [sender._id, receiver.user._id],
                groupCreatedBy: receiver.user._id,
                senderId: sender._id,
                recipientId: receiver.user._id
            }
        };
        this.socketService.joinPrivateGroupRoom(roomInfo);
    }

    presentActionSheet() {
        // const actionSheet = this.actionSheetCtrl.create({
        //     title: 'Select Image Source',
        //     buttons: [
        //         {
        //             text: 'Load from Library',
        //             handler: () => {
        //                 // this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        //             }
        //         },
        //         {
        //             text: 'Use Camera',
        //             handler: () => {
        //                 // this.takePicture(this.camera.PictureSourceType.CAMERA);
        //             }
        //         },
        //         {
        //             text: 'Use Image Picker',
        //             handler: () => {
        //                 // this.loadPictures();
        //             }
        //         },
        //         {
        //             text: 'Cancel',
        //             role: 'cancel'
        //         }
        //     ]
        // });
        // actionSheet.present();
    }

    sendMessage() {
        const message = {
            senderId: this.senderUser._id,
            senderName: this.senderUser.name,
            senderProfilePic: this.senderUser.profilePicLink,
            recipientId: this.receiverUser.user._id,
            recipientName: this.receiverUser.name,
            messageText: this.userInput,
            groupId: this.currentGroupId,
            conversationId: this.conversationId
        };
        const roomInfo = this.chatRoom.roomInfo;
        console.log('Sending message to private chat: ', JSON.stringify(message));
        this.socketService.sendPrivateChatMessage(message, roomInfo);
        this.userInput = '';
    }

    resize() {

    }

    ngOnInit(): void {
        console.log('chat ngOnInit');
    }

    ngOnDestroy() {
        this.socketService.leaveRoom(this.chatRoom.roomInfo);
    }
}
