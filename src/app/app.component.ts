import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {StorageService} from '../services/storage.service';
import {Router} from '@angular/router';
import {SocketService} from '../services/socket.service';
import {AuthenticationService} from '../services/authentication.service';
import {UserService} from '../services/user.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storageService: StorageService,
        private router: Router,
        private socketService: SocketService,
        private userService: UserService,
        private authenticationService: AuthenticationService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.storageService.getKeys().then((response) => {
                if (response.indexOf('kodetra_chat_userId') > -1) {
                    this.storageService.getData('kodetra_chat_userId').then((result) => {
                        console.log('getData', result);
                        if (this.authenticationService.isAuthenticated()) {
                            // tslint:disable-next-line:no-shadowed-variable
                            this.authenticationService.getUserProfileByUserId(this.authenticationService.getUserId())
                            // tslint:disable-next-line:no-shadowed-variable
                                .subscribe((response) => {
                                    console.log('getting profile', response);
                                    this.userService.setUserProfile(response.data);
                                    this.router.navigateByUrl('/account');
                                    this.socketService.initPrivateChatSocket();
                                    this.socketService.connection().subscribe((socket) => {
                                        console.log('Private chat connection successfull' + JSON.stringify(socket));
                                        this.socketService.socketLogin(result._id);
                                    }, (error) => {
                                        console.log('Private chat connection unsuccessfull', error);
                                    });
                                }, (error) => {
                                    console.log('Error while creating card name', error);
                                });
                        } else {
                        }
                    });
                }
            }).catch((error) => {
            });
        });
    }
}
