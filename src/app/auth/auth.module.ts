import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {AuthPage} from './auth.page';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ForgetUsernameComponent} from './forget-username/forget-username.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {ComponentsModule} from '../../components/components.module';
import {ModalsModule} from '../../modals/modals.module';

const routes: Routes = [
    {
        path: '',
        component: AuthPage,
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'registration',
                component: RegistrationComponent
            },
            {
                path: 'forget-password',
                component: ForgetPasswordComponent
            },
            {
                path: 'forget-username',
                component: ForgetUsernameComponent
            },
            {
                path: 'reset-password/:token',
                component: ResetPasswordComponent
            }
        ]
    },

];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        ComponentsModule,
        ModalsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [AuthPage,
        LoginComponent,
        RegistrationComponent,
        ForgetPasswordComponent,
        ForgetUsernameComponent,
        ResetPasswordComponent]
})
export class AuthPageModule {
}
