import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Events} from '@ionic/angular';
import {AuthenticationService} from '../../../services/authentication.service';
import {ToastMessageService} from '../../../services/toast-message.service';

@Component({
    selector: 'app-forget-username',
    templateUrl: './forget-username.component.html',
    styleUrls: ['./forget-username.component.scss']
})
export class ForgetUsernameComponent implements OnInit {
    public formGroup: FormGroup;
    private emailPattern = '^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z]{2,4})$)';

    constructor(private fb: FormBuilder,
                private router: Router,
                private events: Events,
                private toastMessageService: ToastMessageService,
                private authenticationService: AuthenticationService) {
        this.formGroup = this.fb.group({
            email: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(40),
                Validators.required, Validators.pattern(this.emailPattern)])],
        }, {updateOn: 'submit'});
    }

    ngOnInit() {
    }

    forgetUserName() {
        const request = {
            email: this.formGroup.value.email
        };
        console.log('forgetUserName', request);
        this.authenticationService.forgetPassword(request).subscribe((result) => {
            console.log('Forget password result ', result);
        }, (error) => {
            this.toastMessageService.presentToast('Email does not exist', 'toast-css-error');
        });
    }
}
