import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Events} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../../services/storage.service';
import {UserService} from '../../../services/user.service';
import {AlertNotificationService} from '../../../services/alert-notification.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {ToastMessageService} from '../../../services/toast-message.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public formGroup: FormGroup;
    private emailPattern = '^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z]{2,4})$)';
    public returnUrl: any;

    constructor(private fb: FormBuilder,
                private router: Router,
                private route: ActivatedRoute,
                private storageProvider: StorageService,
                private userProvider: UserService,
                private toastMessageService: ToastMessageService,
                private events: Events,
                private alertNotificationService: AlertNotificationService,
                private authenticationService: AuthenticationService) {
        this.formGroup = this.fb.group({
            email: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(40),
                Validators.required, Validators.pattern(this.emailPattern)])],
            password: ['', Validators.required],
        });
    }

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => this.returnUrl = params.return || '/account');
    }

    login() {
        if (this.formGroup.valid) {
            const request = {
                email: this.formGroup.value.email,
                password: this.formGroup.value.password,
                appType: 'web',
                appName: 'kodetra_chat'
            };
            this.authenticationService.login(request).subscribe(response => {
                console.log('login Successfully: ', response);
                this.storageProvider.setData('kodetra_chat_userId', response.user._id);
                this.storageProvider.setData('kodetra_chat_user', response.user);
                this.storageProvider.setData('kodetra_chat_token', response.token);
                this.storageProvider.setData('kodetra_chat_visited', true);
                this.storageProvider.setData('kodetra_chat_refresh_token', response.refresh_token);

                localStorage.setItem('kodetra_chat_userId', response.user._id);
                localStorage.setItem('kodetra_chat_user', JSON.stringify(response.user));
                localStorage.setItem('kodetra_chat_token', response.token);
                localStorage.setItem('kodetra_chat_refresh_token', response.refresh_token);
                const role = this.authenticationService.getUser().role;
                this.router.navigateByUrl(this.returnUrl);
            }, error => {
                this.toastMessageService.presentToast('Email or Password is not valid', 'toast-css-error');
            });
        } else {
            this.toastMessageService.presentToast('Email or Password is not valid', 'toast-css-error');
        }
    }

    register() {
        this.router.navigateByUrl('auth/registration');
    }

    forgetPassword() {
        this.router.navigateByUrl('auth/forget-password');
    }

    forgetUserName() {
        this.router.navigateByUrl('auth/forget-username');
    }

    reset() {
        console.log('Resetting the form');
        this.formGroup.reset();
    }
}
