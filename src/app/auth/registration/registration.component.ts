import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Events, ModalController} from '@ionic/angular';

import {ValidationService} from '../../../services/validation.service';
import {ToastMessageService} from '../../../services/toast-message.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {StorageService} from '../../../services/storage.service';
import {UserService} from '../../../services/user.service';
import {CountryComponent} from '../../../modals/country/country.component';
import {PlanService} from '../../../services/plan.service';

declare var google;

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    public formGroup: FormGroup;
    public formSubmitAttempt = false;
    public items: Array<string> = [];
    private addressLocation: any;
    public formattedAddress: any;
    public addressComponents: any;
    public service = new google.maps.places.AutocompleteService();
    public country: any;
    public plan: any;

    constructor(private fb: FormBuilder,
                private router: Router,
                private events: Events,
                private validationService: ValidationService,
                private storageProvider: StorageService,
                private userService: UserService,
                private planService: PlanService,
                public modalController: ModalController,
                private toastMessageService: ToastMessageService,
                private authenticationService: AuthenticationService) {
        this.formGroup = this.fb.group({
            name: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 50,
                null, null))],
            email: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 100, 'email', null))],
            password: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, null, null))],
            country: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 50, null, null))],
            phone: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, 'number', null))],
            agreement: [false, Validators.compose(validationService.getValidatorsFunctionArray(true, 0, 0, null, null))],
        });
        this.initializeItems();
        this.plan = this.planService.getPlanByName('standard')[0];
    }

    ngOnInit() {
    }

    initializeItems() {
        this.items = [];
    }

    reset() {
        console.log('Resetting the form');
        this.formGroup.reset();
        this.formSubmitAttempt = false;
    }

    register() {
        console.log('Form values ', this.formGroup.value);
        console.log('Form ', this.formGroup);
        this.formSubmitAttempt = true;
        if (this.formGroup.valid) {
            const userInfo = {
                name: this.formGroup.value.name,
                email: this.formGroup.value.email,
                password: this.formGroup.value.password,
                phone: this.country.dial_code + this.formGroup.value.phone,
                country: this.country,
                account_type: 'account',
                role: 'user',
                appType: 'web',
                appName: 'kodetra_chat',
            };
            console.log('User Request: ', userInfo);
            this.authenticationService.registration(userInfo).subscribe(response => {
                // this.events.publish('note:login');
                console.log('User Created: ', response);
                this.login(this.formGroup.value.email, this.formGroup.value.password);
            }, error => {
                this.toastMessageService.presentToast('Email or Password is not valid', 'toast-css-error');
            });
        } else {
            console.log('Error while registering user');
            this.validateAllFormFields(this.formGroup);
            this.toastMessageService.presentToast('Please fill the form!!', 'toast-css-error');
        }
    }

    isFieldValid(field: string) {
        return (!this.formGroup.get(field).valid || this.formGroup.get(field).untouched) &&
            (this.formSubmitAttempt);
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
            'has-feedback': this.isFieldValid(field)
        };
    }

    getErrorMessages(elementName) {
        for (const propertyName in this.formGroup.controls[elementName].errors) {
            if (this.formGroup.controls[elementName].errors.hasOwnProperty(propertyName) &&
                this.formGroup.controls[elementName].invalid) {
                const errorName = this.formGroup.controls[elementName].errors[propertyName];
                return this.validationService.getValidatorErrorMessage(propertyName, errorName);
            }
        }
        return null;
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    getItems(ev: any) {
        // set val to the value of the searchbar
        console.log('getItems', ev);
        const val = ev.target.value;
        if (val.length > 0) {
            this.getPredictions(val, (result) => {
                console.log('result', result);
                this.items = result;
            });
        } else {
            // Reset items back to all of the items
            this.initializeItems();
        }
        // // if the value is an empty string don't filter the items
        // if (val && val.trim() != '') {
        //   this.items = this.items.filter((item) => {
        //     return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        //   })
        // }
    }

    getPredictions(query, callback) {
        // perform request. limit results to Australia
        const request = {
            input: query,
            // componentRestrictions: {country: 'br'},
        };
        this.service.getPlacePredictions(request, (predictions, status) => {
            const items = [];
            if (predictions && predictions.length > 0) {
                predictions.forEach((prediction) => {
                    items.push(prediction.description);
                });
            }
            callback(items);
        });
    }

    saveUserLocation(userLocation) {
        // this.common.setUserLocation(userLocation);
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({address: userLocation}, (results, status) => {
            if (status === google.maps.GeocoderStatus.OK) {
                console.log('Geocode  results' + JSON.stringify(results));
                console.log('Geocode  ' + JSON.stringify(results[0].geometry.location));
                const location = {
                    type: 'Point',
                    coordinates: []
                };
                location.coordinates.push(results[0].geometry.location.lng());
                location.coordinates.push(results[0].geometry.location.lat());
                this.addressLocation = location;
                this.formattedAddress = results[0].formatted_address;
                this.addressComponents = results[0];
                console.log('Geocode was successful addressLocation: ' + JSON.stringify(this.addressLocation));
                console.log('Geocode was successful formatted_address: ' + JSON.stringify(this.formattedAddress));
                console.log('Geocode was successful address_components: ' + JSON.stringify(this.addressComponents));
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
                this.addressLocation = {};
            }
        });
        this.formGroup.controls.address.patchValue(userLocation);
        this.initializeItems();
    }

    async openCountrybModal() {
        const modal = await this.modalController.create({
            component: CountryComponent,
            componentProps: {}
        });
        modal.onDidDismiss().then((response: any) => {
            console.log('country choose', response);
            this.formGroup.controls.country.patchValue(response.data.name);
            this.country = response.data;
        });
        return await modal.present();
    }

    login(email, password) {
        const request = {
            email,
            password,
            appType: 'mobile',
            appName: 'kodetra_chat'
        };
        this.authenticationService.login(request).subscribe(response => {
            console.log('login Successfully: ', response);
            this.storageProvider.setData('kodetra_chat_userId', response.user._id);
            this.storageProvider.setData('kodetra_chat_user', response.user);
            this.storageProvider.setData('kodetra_chat_token', response.token);
            this.storageProvider.setData('kodetra_chat_visited', true);
            this.storageProvider.setData('kodetra_chat_refresh_token', response.refresh_token);

            localStorage.setItem('kodetra_chat_userId', response.user._id);
            localStorage.setItem('kodetra_chat_user', JSON.stringify(response.user));
            localStorage.setItem('kodetra_chat_token', response.token);
            localStorage.setItem('kodetra_chat_refresh_token', response.refresh_token);
            // this.events.publish('note:login');
            this.router.navigateByUrl('/account');
        }, error => {
            this.toastMessageService.presentToast('Email or Password is not valid', 'toast-css-error');
        });
    }
}
