import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {FieldErrorMessageComponent} from './field-error-message/field-error-message.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ControlErrorMessageComponent} from './control-error-message/control-error-message';

@NgModule({
    declarations: [MenuItemComponent, FieldErrorMessageComponent, ControlErrorMessageComponent],
    entryComponents: [
        MenuItemComponent, FieldErrorMessageComponent, ControlErrorMessageComponent
    ],
    exports: [MenuItemComponent, FieldErrorMessageComponent, ControlErrorMessageComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
    ]
})
export class ComponentsModule {
}
