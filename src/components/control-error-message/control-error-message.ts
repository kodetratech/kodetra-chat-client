import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ValidationService} from '../../services/validation.service';


/**
 * Generated class for the ControlErrorMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'control-error-message',
    templateUrl: 'control-error-message.html'
})
export class ControlErrorMessageComponent {

    @Input() control: FormControl;

    constructor(private validationService: ValidationService) {
        console.log('Hello ControlErrorMessageComponent Component');

    }


    get errorMessage() {
        for (const propertyName in this.control.errors) {
            if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
                console.log('Hello ControlErrorMessageComponent propertyName', propertyName);
                console.log('Hello ControlErrorMessageComponent value', this.control.errors[propertyName]);
                return this.validationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
            }
        }
        return null;
    }

}
