import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(public auth: AuthenticationService, public router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canActivate checking');
        if (!this.auth.isAuthenticated()) {
            console.log('User Not Authenticated', state.url);
            this.router.navigate(['/auth/login'], {
                queryParams: {
                    return: state.url
                }
            });
            return false;
        }
        return true;
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canActivateChild checking');
        if (!this.auth.isAuthenticated()) {
            console.log('User Not Authenticated');
            this.router.navigate(['/auth/login']);
            return false;
        }
        return true;
    }

}
