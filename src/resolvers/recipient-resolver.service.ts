import {Injectable} from '@angular/core';

import {ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class RecipientResolveService {
    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        const id = route.paramMap.get('id');
        console.log('recipient resolve by id', id);
        return this.authenticationService.getUserProfileByUserId(id);
    }
}
