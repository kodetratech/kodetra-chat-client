import {Injectable} from '@angular/core';

import {ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class SenderResolveService {
    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        console.log('sender resolve by id', this.authenticationService.getUserId());
        return this.authenticationService.getUserProfileByUserId(this.authenticationService.getUserId());
    }
}
