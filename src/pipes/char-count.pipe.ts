import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'charCount'
})
export class CharCountPipe implements PipeTransform {
    transform(text: string, args: number) {
        const maxLength = args || 250;
        const length = text.length;
        return text.length;
    }
}
