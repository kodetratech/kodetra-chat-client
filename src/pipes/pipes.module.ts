import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TextCapitalizePipe} from './text-capitalize.pipe';
import {CharCountPipe} from './char-count.pipe';

@NgModule({
    declarations: [TextCapitalizePipe, CharCountPipe],
    imports: [
        CommonModule
    ],
    exports: [TextCapitalizePipe, CharCountPipe]
})
export class PipesModule {
}
