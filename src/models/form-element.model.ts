export class FormElement {
    public elementType: string;
    public elementName: string;
    public placeHolder: string;
    public fieldClass: string;
    public fieldLabel: string;

    constructor(elType, elName, plHolder, fldClass, fldLabel) {
        this.elementType = elType;
        this.elementName = elName;
        this.placeHolder = plHolder;
        this.fieldClass = fldClass;
        this.fieldLabel = fldLabel;
    }
}
