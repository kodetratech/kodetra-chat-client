import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GooglePlacesDirective } from './google-places.directive';

@NgModule({
  declarations: [GooglePlacesDirective],
  imports: [
    CommonModule
  ]
})
export class DirectivesModule { }
