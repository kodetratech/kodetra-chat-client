import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FileUploadWebService} from '../../services/file-upload-web.service';
import {UserService} from '../../services/user.service';
import {ModalController, NavParams} from '@ionic/angular';
import {ToastMessageService} from '../../services/toast-message.service';

@Component({
    selector: 'app-file-upload-web',
    templateUrl: './file-upload-web.component.html',
    styleUrls: ['./file-upload-web.component.scss']
})
export class FileUploadWebComponent implements OnInit {

    profileForm: FormGroup;
    error: string;
    public type: any;
    fileUpload = {status: '', message: '', filePath: ''};

    constructor(private fb: FormBuilder,
                private userService: UserService,
                private toastMessageService: ToastMessageService,
                private navParams: NavParams,
                public modalCtrl: ModalController,
                private fileUploadWebService: FileUploadWebService) {
        this.type = this.navParams.get('type');
    }

    ngOnInit() {
        this.profileForm = this.fb.group({
            // name: [''],
            file: ['']
        });
    }

    onSelectedFile(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.profileForm.get('file').setValue(file);
        }
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('userId', this.userService.getUserId());
        formData.append('appName', 'bizcard');
        formData.append('fileType', 'image');
        formData.append('file', this.profileForm.get('file').value);

        this.fileUploadWebService.upload(formData).subscribe(
            (res) => {
                this.fileUpload = res;
                console.log('fileUpload', this.fileUpload);
                if (res.data && res.data.message) {
                    this.modalCtrl.dismiss(res.data);
                    this.toastMessageService.presentToast(res.data.message, 'toast-css-success');
                }
            },
            (err) => {
                this.error = err;
                this.toastMessageService.presentToast('File Upload Failed', 'toast-css-error');
            }
        );
    }

    closeModal() {
        this.modalCtrl.dismiss(null);
    }
}
