import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadWebComponent } from './file-upload-web.component';

describe('FileUploadWebComponent', () => {
  let component: FileUploadWebComponent;
  let fixture: ComponentFixture<FileUploadWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
