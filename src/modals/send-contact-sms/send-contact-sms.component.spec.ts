import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendContactSmsPage } from './send-contact-sms.page';

describe('SendContactSmsPage', () => {
  let component: SendContactSmsPage;
  let fixture: ComponentFixture<SendContactSmsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendContactSmsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendContactSmsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
