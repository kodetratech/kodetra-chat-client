import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkPlaceComponent} from './work-place/work-place.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {IonicModule} from '@ionic/angular';
import {MomentModule} from 'angular2-moment';
import {FileUploadWebComponent} from './file-upload-web/file-upload-web.component';
import {ComponentsModule} from '../components/components.module';
import {SendContactSmsComponent} from './send-contact-sms/send-contact-sms.component';
import {CountryComponent} from './country/country.component';
import {PipesModule} from '../pipes/pipes.module';
import {ImageLightboxComponent} from './image-lightbox/image-lightbox.component';

@NgModule({
    declarations: [
        WorkPlaceComponent,
        SendContactSmsComponent,
        FileUploadWebComponent,
        ImageLightboxComponent,
        CountryComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentsModule,
        ReactiveFormsModule,
        SharedModule,
        PipesModule,
        MomentModule
    ],
    entryComponents: [
        WorkPlaceComponent,
        SendContactSmsComponent,
        FileUploadWebComponent,
        ImageLightboxComponent,
        CountryComponent]
})
export class ModalsModule {
}
