import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-image-lightbox',
    templateUrl: './image-lightbox.component.html',
    styleUrls: ['./image-lightbox.component.scss'],
})
export class ImageLightboxComponent implements OnInit {

    @ViewChild('slider', {static: false, read: ElementRef}) slider: ElementRef;
    img: any;

    sliderOpts = {
        zoom: {
            maxRatio: 5
        }
    };

    constructor(private navParams: NavParams, private modalController: ModalController) {
    }

    ngOnInit() {
        this.img = this.navParams.get('img');
    }

    zoom(zoomIn: boolean) {
        const zoom = this.slider.nativeElement.swiper.zoom;
        if (zoomIn) {
            zoom.in();
        } else {
            zoom.out();
        }
    }

    close() {
        this.modalController.dismiss();
    }
}
