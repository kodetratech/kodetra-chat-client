export const environment = {
    production: true,
    hostName: 'https://localhost:3000',
    appUrl: '/api/v1/chat',
};
