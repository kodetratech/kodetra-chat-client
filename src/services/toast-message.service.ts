import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToastMessageService {

    constructor(public toastCtrl: ToastController) {
    }

    async presentToast(message, cssClass) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top',  // 'top','bottom','middle'
            showCloseButton: false,
            cssClass: cssClass,
            closeButtonText: 'Close',
        });
        toast.present();
    }
}
