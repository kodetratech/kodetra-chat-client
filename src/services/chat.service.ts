import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';
import {throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ChatService {
    private baseUrl = environment.hostName + environment.appUrl;

    constructor(public http: HttpClient, private authenticationService: AuthenticationService) {
    }

    getHeader() {
        return new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.authenticationService.getToken());
    }


    getPreviousChat(request) {
        return this.http.post(this.baseUrl + '/connection/previous/chat', request, {
            headers: this.getHeader(),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }

    private extractData(res: HttpResponse<any>) {
        // let headers = new Headers(res.headers);
        // console.log("headers data loaded " , headers);
        return {
            data: res.body.data,
            total: res.headers.get('X-Total-Docs'),
            limit: res.headers.get('X-Limit'),
            page: res.headers.get('X-Page'),
            pages: res.headers.get('X-Pages'),
        };
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return throwError(error);
    }
}
