import {Injectable} from '@angular/core';
import {ValidatorFn, Validators} from '@angular/forms';
import {Dictionary} from '../util/helper/dictionary';

@Injectable({
    providedIn: 'root'
})
export class ValidationService {

    public regEx = new Dictionary<RegExp>();

    constructor() {
        this.regEx.Add('email', new RegExp('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z]{2,4})$)'));
        this.regEx.Add('number', new RegExp('^\\d+$'));
        this.regEx.Add('word', new RegExp('^[a-zA-Z]{1}[a-zA-Z\\s\\-\\\']*$'));
        this.regEx.Add('name', new RegExp('^[a-zA-Z]{1}[a-zA-ZàáâäãåąčćęèéêëėįìíîïñńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,\'-]+$'));
        this.regEx.Add('middleName', new RegExp('^[a-zA-Z]{1}[a-zA-Z\-\']*$'));
        this.regEx.Add('address', new RegExp('^[a-zA-Z0-9\\s\\.\\,\\#\\/\\-\\:\\&\\\']+$'));
        this.regEx.Add('city', new RegExp('^[a-zA-Z\\s\\.\\-\\\']+$'));
        this.regEx.Add('states', new RegExp('(AA|AE|AK|AL|AP|AR|AS|AZ|CA|CO|CT|DC|DE|FL|FM|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MH|MI|MN|MO|MP|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|PW|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY)'));
        this.regEx.Add('zip', new RegExp('^\\d{6}$'));
        this.regEx.Add('phone', new RegExp('\\([2-9]\\d{2}\\)\\s\\d{3}\\s\\-\\s\\d{4}$'));
        this.regEx.Add('ssn', new RegExp('\\b(?!000)(?!9)(?:[0-8]\\d{2})\\s\\-\\s\\d{2}(?!0000)\\s\\-\\s\\d{4}\\b'));
        this.regEx.Add('password', new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^+\\-\(\)])((?![<>"&\\\\/]).){7,50}$'));
        this.regEx.Add('dateOfBirth', new RegExp('^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$'));
        this.regEx.Add('website', new RegExp('^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&\'\\(\\)\\*\\+,;=.]+$'));
    }

    public getValidatorsFunctionArray(required: boolean, minLength: number, maxLength: number, pattern: string, customValidatorArray): Array<ValidatorFn> {
        const ValidatorsFunctionArray: Array<ValidatorFn> = [];
        if (required) {
            ValidatorsFunctionArray.push(Validators.required);
        }
        if (minLength && minLength > 0) {
            ValidatorsFunctionArray.push(Validators.minLength(minLength));
        }
        if (maxLength && maxLength > 0) {
            ValidatorsFunctionArray.push(Validators.maxLength(maxLength));
        }
        if (pattern) {
            ValidatorsFunctionArray.push(Validators.pattern(this.regEx.Item(pattern)));
        }
        if (customValidatorArray) {
            for (let i = 0; i < customValidatorArray.length; i++) {
                ValidatorsFunctionArray.push(customValidatorArray[i]);
            }
        }
        return ValidatorsFunctionArray;

    }

    public getValidatorErrorMessage(validatorName: string, validatorValue?: any): string {
        const config = {
            'required': 'Required',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidEmailAddress': 'Invalid email address',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'minlength': `Minimum length ${validatorValue.requiredLength}`,
            'maxlength': `Maximum length ${validatorValue.requiredLength}`,
            'pattern': 'Invalid Pattern',
            'invalidFirstName': 'Invalid First Name'
        };

        return config[validatorName];
    }

}
