import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import * as io from 'socket.io-client';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SocketService {

    private socket: any;
    private socketUrl: any;
    private socketStatus = false;

    public chatGroups: Array<any> = [];

    constructor(public http: HttpClient) {
        console.log('Hello SocketProvider Provider');
        this.initPrivateChatSocket();
    }

    initPrivateChatSocket() {
        this.socketUrl = environment.hostName;
        this.socket = io.connect(this.socketUrl);
        this.socketStatus = this.socket.connected;
        this.socket.on('disconnect', () => this.disconnect());
        this.socket.on('chat-error', (error: string) => {
            console.log(`ERROR : "${error}" (${this.socketUrl})`);
        });
        this.socket.on('groupOnlineUsers', (data) => {
            console.log('groupOnlineUsers : ', data);
        });
    }

    leaveRoom(roomInfo) {
        console.log('User is going to leaving romm : ', roomInfo);
        this.socket.emit('chat-private-group-leave', {
            roomInfo,
        }, () => {
            console.log('user left the room');
        });
    }

    joinPrivateGroupRoom(roomInfo) {
        console.log('User is going to join romm : ', roomInfo);
        this.socket.emit('chat-join-private-chat', {
            roomInfo,
        }, () => {
            console.log('user joined the room');
        });
    }

    sendPrivateChatMessage(message, roomInfo) {
        this.socket.emit('chat-send-private-chat-msg', {
            roomInfo,
            message
        }, () => {
        });
    }

    socketLogin(userId) {
        console.log('socketLogin', userId);
        this.socket.emit('login', {
            userId,
        }, () => {
        });
    }

    privateGroupCreated() {
        let observable: Observable<any>;
        observable = new Observable(observer => {
            this.socket.on('chat-private-group-created', (data) => {
                console.log('private group created : ', data);
            });
            return () => {
                // this.socket.disconnect();
            };
        });
        return observable;
    }

    privateGroupJoined() {
        let observable: Observable<any>;
        observable = new Observable(observer => {
            this.socket.on('chat-private-chat-joined', (data) => {
                console.log('private group joined : ', JSON.stringify(data));
                observer.next(data);
            });
            return () => {
                // this.socket.disconnect();
            };
        });
        return observable;
    }


    receivePrivateChatMessage() {
        let observable: Observable<any>;
        observable = new Observable(observer => {
            this.socket.on('chat-private-group-msg-receive', (response) => {
                console.log('Chat Message Received', response);
                observer.next(response);
            });
            return () => {
                // this.socket.disconnect();
            };
        });
        return observable;
    }


    connection(): Observable<any> {
        let observable: Observable<any>;
        observable = new Observable(observer => {
            this.socket.on('connect', () => {
                observer.next({success: true});
            });
            return () => {
                // this.socket.disconnect();
            };
        });
        return observable;
    }


    // Handle connection closing
    private disconnect() {
        console.log('Disconnected from');
    }

}
