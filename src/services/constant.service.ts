import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Platform} from '@ionic/angular';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConstantService {

    public geolocationPosition: any;
    private baseUrl = environment.hostName + environment.appUrl;
    private cityList: any = [];
    private storeFilter: any = {
        city: '',
        category: [],
        distance: 50,
        benefits: []
    };

    constructor(public http: HttpClient,
                private platform: Platform,
                private geolocation: Geolocation) {

    }

    setUserLocation(userLocation) {
        this.geolocationPosition = userLocation;
    }

    getUserLocation() {
        return this.geolocationPosition;
    }

    isMobilePlatform() {
        return this.platform.is('cordova') && (this.platform.is('android') || this.platform.is('ios'));
    }

    // getUserLocationBrowser() {
    //     const option: GeolocationOptions = {
    //         enableHighAccuracy: true
    //     };
    //     if (this.platform.is('cordova')) {
    //         this.geolocation.getCurrentPosition(option).then((position) => {
    //             // alert(JSON.stringify(resp));
    //             this.geolocationPosition = position;
    //         }).catch((error) => {
    //             console.error('Error getting location', error);
    //         });
    //         const watch = this.geolocation.watchPosition();
    //         watch.subscribe((data) => {
    //             if (data && data.coords && data.coords.latitude && data.coords.longitude) {
    //                 this.geolocationPosition = data;
    //             }
    //
    //             // data can be a set of coordinates, or an error (if an error occurred).
    //             // data.coords.latitude
    //             // data.coords.longitude
    //         });
    //     } else {
    //         if (window.navigator && window.navigator.geolocation) {
    //             window.navigator.geolocation.getCurrentPosition(
    //                 (position) => {
    //                     this.geolocationPosition = position;
    //                     console.log('result position', position);
    //                     // alert(JSON.stringify(position.coords.latitude + '  ' + position.coords.longitude));
    //                     // this.registrationProvider.getAddress(position.coords.latitude, position.coords.longitude).subscribe((result) => {
    //                     //     console.log('result getAddress', result);
    //                     // }, (error) => {
    //                     //     console.error('error getAddress', error);
    //                     // });
    //                 },
    //                 (error) => {
    //                     switch (error.code) {
    //                         case 1:
    //                             console.log('Permission Denied');
    //                             break;
    //                         case 2:
    //                             console.log('Position Unavailable');
    //                             break;
    //                         case 3:
    //                             console.log('Timeout');
    //                             break;
    //                     }
    //                 },
    //                 option
    //             );
    //         }
    //     }
    //
    // }

    // getUserLocationInformation() {
    //     if (this.platform.is('cordova')) {
    //         const option: GeolocationOptions = {
    //             enableHighAccuracy: true
    //         };
    //         this.geolocation.getCurrentPosition(option).then((position) => {
    //             // alert(JSON.stringify(resp));
    //             this.geolocationPosition = position;
    //         }).catch((error) => {
    //             console.error('Error getting location', error);
    //         });
    //         const watch = this.geolocation.watchPosition();
    //         watch.subscribe((data) => {
    //             if (data && data.coords && data.coords.latitude && data.coords.longitude) {
    //                 this.geolocationPosition = data;
    //             }
    //         });
    //     }
    // }
    //
    // public InitializeApplication() {
    //     this.getUserLocationInformation();
    //     this.registrationProvider.getCityAndCategory().subscribe((response) => {
    //         const citySet = new Set();
    //         if (response.data) {
    //             response.data.forEach((store) => {
    //                 if (store && store.address_components && store.address_components.county) {
    //                     citySet.add(store.address_components.county);
    //                 }
    //                 this.setCityList(Array.from(citySet));
    //             });
    //         }
    //     }, (error) => {
    //         console.error('city and category error', error);
    //     });
    // }

    public upload(files: Set<any>, userId, token, type): { [key: string]: Observable<number> } {
        // this will be the our resulting map
        const status = {};
        let url: any = this.baseUrl + '/store-profile/update/logo';
        if (type && type === 'logo') {
            url = this.baseUrl + '/store-profile/update/logo';
        } else {
            url = this.baseUrl + '/store-profile/update/cover/photo';
        }
        files.forEach(file => {
            // create a new multipart-form for every file
            const formData: FormData = new FormData();
            formData.append('appName', 'convenix');
            formData.append('file', file, file.name);
            formData.append('user', userId);
            formData.append('token', token);
            formData.append('type', type);
            // create a http-post request and pass the form
            // tell it to report the upload progress
            const req = new HttpRequest('POST', url, formData, {
                reportProgress: true
            });

            // create a new progress-subject for every file
            const progress = new Subject<number>();

            // send the http-request and subscribe for progress-updates

            // const startTime = new Date().getTime();
            this.http.request(req).subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {
                    // calculate the progress percentage

                    const percentDone = Math.round((100 * event.loaded) / event.total);
                    // pass the percentage into the progress-stream
                    progress.next(percentDone);
                } else if (event instanceof HttpResponse) {
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    // const result = {
                    //   data: event.body,
                    // };
                    progress.complete();
                }
            });

            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable()
            };
        });

        // return the map of progress.observables
        return status;
    }
}
