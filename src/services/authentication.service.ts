import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../environments/environment';
import {StorageService} from './storage.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private baseUrl = environment.hostName + environment.appUrl;

    constructor(public http: HttpClient, private storageService: StorageService) {
    }

    getHeader() {
        return new HttpHeaders().set('Authorization', 'Bearer ' + this.getToken());
    }

    getToken() {
        const token = localStorage.getItem('kodetra_chat_token');
        if (token) {
            return token;
        } else {
            return null;
        }
    }

    getUserId() {
        const userId = localStorage.getItem('kodetra_chat_userId');
        if (userId) {
            return userId;
        } else {
            return null;
        }
    }

    getUser() {
        const user = localStorage.getItem('kodetra_chat_user');
        if (user) {
            return JSON.parse(user);
        } else {
            return null;
        }
    }

    getUserRole() {
        const user = JSON.parse(localStorage.getItem('kodetra_chat_user'));
        if (user) {
            return user.role;
        } else {
            return null;
        }
    }

    public getRefreshToken() {
        const token = localStorage.getItem('kodetra_chat_refresh_token');
        if (token) {
            return token;
        } else {
            return null;
        }
    }

    isAuthenticated() {
        const token = this.getToken();
        const userId = this.getUserId();
        return !!(token && userId);
    }


    refreshToken() {
        const data = {
            id: this.getUserId(),
            refresh_token: this.getRefreshToken()
        };
        return this.http.post(this.baseUrl + '/refresh/access-token', data)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                console.log('refresh token response', JSON.stringify(response));
                localStorage.setItem('kodetra_chat_token', response.token);
                localStorage.setItem('kodetra_chat_refresh_token', response.refresh_token);
                return response;
            }).catch(this.handleError);
    }

    loginByFacebook() {
        location.href = environment.hostName + '/auth/facebook';
    }

    loginByGoogle() {
        location.href = environment.hostName + '/auth/google';
    }

    loginByTwitter() {
        location.href = environment.hostName + '/auth/twitter';
    }

    login(request): Observable<any> {
        return this.http.post(this.baseUrl + '/user/login', request)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                const loginResponse = response.data;
                const token = loginResponse.token;
                if (token) {
                    // set token property
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('kodetra_chat_userId', loginResponse.user._id);
                    localStorage.setItem('kodetra_chat_user', JSON.stringify(loginResponse.user));
                    localStorage.setItem('kodetra_chat_token', loginResponse.token);
                    // return true to indicate successful login
                    return loginResponse;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }).map((loginResponse: any) => {
                return loginResponse;
            });
    }

    registration(userData): Observable<any> {
        return this.http.post(this.baseUrl + '/user/create', userData)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                return response;
            }).catch(this.handleError);
    }

    updateResume(user, userData): Observable<any> {
        return this.http.put(this.baseUrl + '/resume/update/resume/' + user, userData)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                return response;
            }).catch(this.handleError);
    }

    forgetPassword(request): Observable<any> {
        return this.http.post(this.baseUrl + '/user/forget/password', request)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                return response;
            }).catch(this.handleError);
    }

    resetPassword(request): Observable<any> {
        return this.http.post(this.baseUrl + '/user/reset/password', request)
            .map((response: any) => {
                // login successful if there's a jwt token in the response
                return response;
            }).catch(this.handleError);
    }


    registerUserDevice(device): Observable<any> {
        return this.http.post(environment.hostName + '/api/v1/util/device/register', {device: device}, {
            headers: this.getHeader(),
            observe: 'response',
            withCredentials: false
        })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getUserProfileByUserId(userId) {
        return this.http.get(this.baseUrl + '/user/profile/' + userId, {
            headers: this.getHeader(),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }


    getUsersList(userId) {
        return this.http.get(this.baseUrl + '/user/profiles/' + userId, {
            headers: this.getHeader(),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }

    logout() {
        localStorage.clear();
        this.storageService.clearStore();
    }

    private extractData(res: HttpResponse<any>) {
        // let headers = new Headers(res.headers);
        // console.log("headers data loaded " , headers);
        return {
            data: res.body.data,
            total: res.headers.get('X-Total-Docs'),
            limit: res.headers.get('X-Limit'),
            page: res.headers.get('X-Page'),
            pages: res.headers.get('X-Pages'),
        };
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(error);
    }
}
