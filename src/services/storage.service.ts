import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {


    constructor(public http: HttpClient, private storage: Storage) {
        console.log('Hello StorageProvider Provider');
    }


    async getData(key: string) {
        return await this.storage.get(key);
    }

    getKeys() {
        return this.storage.keys();
    }

    setData(key: string, data: any) {
        return this.storage.set(key, data);
    }

    async removeData(key: string) {
        return await this.storage.remove(key);
    }

    clearStore() {
        return this.storage.clear();
    }
}
