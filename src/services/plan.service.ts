import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';
import {throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PlanService {
    private baseUrl = environment.hostName + environment.appUrl;
    private plans: Array<any> = [];

    constructor(public http: HttpClient, private authenticationService: AuthenticationService) {
    }

    getHeader() {
        return new HttpHeaders()
        // .set('X-Requested-With', null)
        // .set('If-None-Match', null)
            .set('Authorization', 'Bearer ' + this.authenticationService.getToken());
    }


    setPlans(plans) {
        this.plans = plans;
    }

    getPlans() {
        return this.plans;
    }

    getPlanByName(planName) {
        return this.plans.filter((plan) => {
            return plan.plan_name.toLowerCase() === planName.toLowerCase();
        });
    }

    loadPlans() {
        return this.http.get(this.baseUrl + '/plan', {
            headers: this.getHeader(),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }

    private extractData(res: HttpResponse<any>) {
        // let headers = new Headers(res.headers);
        // console.log("headers data loaded " , headers);
        return {
            data: res.body.data,
            total: res.headers.get('X-Total-Docs'),
            limit: res.headers.get('X-Limit'),
            page: res.headers.get('X-Page'),
            pages: res.headers.get('X-Pages'),
        };
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return throwError(error);
    }
}
