import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class SpinnerLoaderService {

    public loading: any;

    constructor(public loadingCtrl: LoadingController) {
        console.log('Hello SpinnerLoaderProvider Provider');
    }

    async startSpinner(msg: string) {
        // Dismiss previously created loading
        if (this.loading && this.loading !== null) {
            this.loading.dismiss();
        }
        this.loading = await this.loadingCtrl.create({
            message: msg,
            spinner: 'crescent',
        });
        return await this.loading.present();
    }

    stopSpinner() {
        if (this.loading && this.loading !== null) {
            this.loading.dismiss();
            this.loading = null;
        }
    }
}
