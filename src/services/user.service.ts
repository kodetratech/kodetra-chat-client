import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {AuthenticationService} from './authentication.service';
import {BehaviorSubject} from 'rxjs';
import {Dictionary} from '../util/helper/dictionary';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    public user: any;
    public resume: any;
    private userProfile: any;
    private chatRecipientUser: any;
    private connectionProfile: any;
    public connectionsList = new Dictionary<any>();
    public profileScore = new BehaviorSubject<any>(0);

    constructor(private storageProvider: StorageService,
                private authenticationService: AuthenticationService) {
    }

    setConnectionProfile(profile) {
        this.connectionProfile = profile;
    }

    getConnectionProfile() {
        return this.connectionProfile;
    }

    setChatRecipient(recipient) {
        this.chatRecipientUser = recipient;
    }

    getChatRecipient() {
        return this.chatRecipientUser;
    }

    setUserProfile(profile) {
        this.userProfile = profile;
        this.user = profile.user;
        // this.resume = profile.resume;
        // this.calculateProfileScore();
    }

    getUserProfile() {
        return this.userProfile;
    }


    setUser(user) {
        this.user = user;
    }

    getUser() {
        return this.user;
    }

    setResume(resume) {
        this.resume = resume;
        // this.calculateProfileScore();
    }

    getResume() {
        return this.resume;
    }

    getUserId() {
        if (this.user && this.user._id) {
            return this.user._id;
        } else {
            this.authenticationService.getUserId();
        }

    }

    setConnections(connections) {
        console.log('connections', connections);
        connections.forEach((connection) => {
            this.connectionsList.Add(connection.user._id, connection);
        });
    }

    getConnectionList() {
        return this.connectionsList.Values();
    }

    getConnection(userId) {
        console.log('contains key user id', userId);
        console.log('contains key', this.connectionsList.ContainsKey(userId));
        if (this.connectionsList.ContainsKey(userId)) {
            return this.connectionsList.Item(userId);
        }
        return null;
    }

    // calculateProfileScore() {
    //     let score = 0;
    //     if (this.resume) {
    //         if (this.resume.full_name && this.resume.full_name.toLowerCase() !== 'not available' && this.resume.full_name.length > 0) {
    //             score += 5;
    //         }
    //         if (this.resume.title && this.resume.title.toLowerCase() !== 'not available' && this.resume.title.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.email && this.resume.email.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.phone && this.resume.phone.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.company && this.resume.company.toLowerCase() !== 'not available' && this.resume.company.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.summary && this.resume.summary.toLowerCase() !== 'not available' && this.resume.summary.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.work_industry && this.resume.work_industry.toLowerCase() !== 'not available' &&
    //             this.resume.work_industry.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.year_of_experience && this.resume.year_of_experience.toLowerCase() !== 'not available' &&
    //             this.resume.year_of_experience.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.currentGoal && this.resume.currentGoal.length > 0) {
    //             score += 10;
    //         }
    //
    //         if (this.resume.interest && this.resume.interest.length > 0) {
    //             score += 10;
    //         }
    //
    //         // if (this.resume.skills) {
    //         //     score += 5;
    //         // }
    //
    //         if (this.resume.experience && this.resume.experience.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.education && this.resume.education.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.awards && this.resume.awards.length > 0) {
    //             score += 5;
    //         }
    //
    //         if (this.resume.social_media && this.resume.social_media.length > 0) {
    //             score += 5;
    //         }
    //         if (this.resume.address && this.resume.address.toLowerCase() !== 'not available' &&
    //             this.resume.address.length > 0) {
    //             score += 10;
    //         }
    //
    //     }
    //     console.log('Profile score', score);
    //     this.profileScore.next(score);
    // }
}
