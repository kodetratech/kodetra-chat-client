import {Injectable} from '@angular/core';
import {Platform} from '@ionic/angular';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UtilService {
    public geolocationPosition: any;
    private enrollmentProgress = 0;

    constructor(public http: HttpClient,
                private platform: Platform) {
    }

    setEnrollmentProgess(stepCount) {
        this.enrollmentProgress = 1.6 * stepCount;
    }

    getEnrollmentProgess() {
        return this.enrollmentProgress;
    }

    getUserLocation() {
        return this.geolocationPosition;
    }

    isMobilePlatform() {
        return this.platform.is('cordova');
    }

    getUserLocationBrowser() {
        // if (this.platform.is('cordova')) {
        //     const option: GeolocationOptions = {
        //         enableHighAccuracy: true
        //     };
        //     this.geolocation.getCurrentPosition(option).then((position) => {
        //         console.log('Current location' + JSON.stringify(position));
        //         // alert(JSON.stringify(resp));
        //         this.geolocationPosition = position;
        //     }).catch((error) => {
        //         console.log('Error getting location', error);
        //     });
        // } else {
        //     const option: GeolocationOptions = {
        //         enableHighAccuracy: true
        //     };
        //
        // }
        if (window.navigator && window.navigator.geolocation) {
            // @ts-ignore
            window.navigator.geolocation.getCurrentPosition().then(position => {
                this.geolocationPosition = position;
                console.log(position);
                // alert(JSON.stringify(position.coords.latitude + '  ' + position.coords.longitude));
            }, error => {
                switch (error.code) {
                    case 1:
                        console.log('Permission Denied');
                        break;
                    case 2:
                        console.log('Position Unavailable');
                        break;
                    case 3:
                        console.log('Timeout');
                        break;
                }
            });
        }
    }
}
