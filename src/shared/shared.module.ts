import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {TextMaskModule} from 'angular2-text-mask';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MomentModule} from 'angular2-moment';
import {NgCircleProgressModule} from 'ng-circle-progress';

@NgModule({
    declarations: [],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        NgxQRCodeModule,
        MomentModule,
        TextMaskModule,
        NgCircleProgressModule.forRoot({
            // set defaults here
            radius: 100,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            outerStrokeColor: '#78C000',
            innerStrokeColor: '#C7E596',
            animationDuration: 300,
        }),
    ],
    exports: [NgxQRCodeModule, TextMaskModule, MomentModule, NgCircleProgressModule]
})
export class SharedModule {
}
