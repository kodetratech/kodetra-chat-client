import {FormControl} from '@angular/forms';

export class UsernameValidator {
    static validUsername(fc: FormControl) {
        console.log('we came here for use validation');
        if (fc.value.match(/^[a-zA-Z]{1}[a-zA-ZàáâäãåąčćęèéêëėįìíîïñńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,'-]+$/)) {
            return (null);
        } else {
            return {invalidFirstName: true};
        }
    }
}
